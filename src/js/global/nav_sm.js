/**
 * Main menu triggering
 */

(function(){
  var menuTrigger = document.querySelector('[data-menu-trigger]');

  menuTrigger.addEventListener('click', function (e) {
    document.body.classList.toggle('show-menu-main');
  });
})();
